package org.MOUHSINI.main;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.MOUHSINI.model.Commune;

public class put {

	public static void main(String[] args) {

		
		Client client = ClientBuilder.newClient();
//		WebTarget target = client.target("http://localhost:8080/tp-java-ee/rest");
//		WebTarget communeupdate = target.path("communes/update");
//		Commune paris = new Commune("75000","paris");		
//		Invocation.Builder builder = communeupdate.request();
//		Entity<Commune> entity = Entity.entity(paris, MediaType.APPLICATION_JSON);
//		builder.put(entity);
		
		
		WebTarget targetToCommuneID = client.target("http://localhost:8080/tp-java-ee/rest/communes/{codepostal}");
		WebTarget putTarget = targetToCommuneID.resolveTemplate("codepostal", "75000");
		
		Form form = new Form();
		form.param("codepostal", "75000");
		form.param("nom", "test");
		
		Entity<Form> formEntity = Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED);
		Response response = putTarget.request().put(formEntity);
		int status = response.getStatus();
		System.out.println(status);
		Commune commune =(Commune) response.readEntity(Commune.class);
		System.out.println(commune);
	}

}
