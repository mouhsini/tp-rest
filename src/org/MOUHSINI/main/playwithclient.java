package org.MOUHSINI.main;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBException;

import org.MOUHSINI.model.User;

public class playwithclient {

	public static void main(String[] args) throws JAXBException {
		
		Client client = ClientBuilder.newClient();
		
		WebTarget host = client.target("http://localhost:8080/tp-java-ee/rest");
		WebTarget userGetpath = host.path("user/{id}");
		WebTarget userPostpath = host.path("user");
	
		WebTarget requestTarget = userGetpath.resolveTemplate("id", 15);
		
		User user = new User(17,"polo",38);
		Entity<User> entity = Entity.entity(user, MediaType.APPLICATION_JSON);
		
		Response response = requestTarget.request().post(entity);
		
		int status = response.getStatus();
		System.out.println("statuc = "+status);
		
	}
}
