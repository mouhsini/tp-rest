package org.MOUHSINI.main;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.MOUHSINI.model.User;


public class main {

	public static void main(String[] args) throws JAXBException {

		
		JAXBContext jaxcontext = JAXBContext.newInstance(User.class);
		
		Marshaller marshaller = jaxcontext.createMarshaller();
	
		User user = new User(16,"Ngolo",28);
		marshaller.setProperty("jaxb.encoding","utf-8");
		marshaller.setProperty("jaxb.formatted.output",true);
		marshaller.marshal(user, new File("xml/user.xml"));
		
		Unmarshaller unmarshaller = jaxcontext.createUnmarshaller();
		User newuser = (User) unmarshaller.unmarshal(new File("xml/user.xml"));
		System.out.println("user = "+ newuser);
		
		
	}

}
