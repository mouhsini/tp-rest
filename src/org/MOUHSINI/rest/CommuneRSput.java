package org.MOUHSINI.rest;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.MOUHSINI.cdi.CommuneService;
import org.MOUHSINI.model.Commune;

@Path("communes")
public class CommuneRSput {
	@Inject
	private CommuneService communeservice;

	@PUT
	@Path("{codepostal}")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.TEXT_XML)
	public Response update(@FormParam("codepostal") String codepostal, @FormParam("nom") String nom) {
		Commune c = new Commune(codepostal, nom);
		communeservice.create(c);
		return Response.status(Status.CREATED).entity(c).build();
	}

	@DELETE
	@Path("delete/{codepostal}")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.TEXT_XML)
	public Response delete(@PathParam("codepostal") String codepostal) {
		Commune c = communeservice.findById(codepostal);
		communeservice.create(c);
		return Response.status(201).entity(c).build();
	}

}
