package org.MOUHSINI.rest;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.MOUHSINI.cdi.CommuneService;
import org.MOUHSINI.ejb.CommuneEJB;
import org.MOUHSINI.model.Commune;
import org.MOUHSINI.model.User;

@Path("commune")
public class CommuneRS {

	@Inject
	private CommuneService communeservice;
	
	@GET @Path("{codepostal}")
	@Produces(MediaType.TEXT_XML)
	public Response findById(
			@PathParam("codepostal") String codepostal)
	{
		
	Commune commune = communeservice.findById(codepostal);	
	if(commune == null)
	{
		return Response.status(404).build();
	}else {
		return Response
				.ok(commune)
				.status(200)
				.build();}
	}
	
	@POST @Path("create")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response create(@FormParam("codepostal") String codepostal,
			@FormParam("nom") String nom, @FormParam("population") long population) {
		
		Commune c = new Commune(codepostal,nom,population);
		Commune commune = communeservice.findById(codepostal);	
		if(commune == null) {
		communeservice.create(c);
		return Response.status(Status.CREATED).entity(c).build();}
		else
			return Response.status(Status.BAD_REQUEST).build();
	}
	
}
