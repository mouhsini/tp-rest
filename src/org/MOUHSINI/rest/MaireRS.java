package org.MOUHSINI.rest;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.MOUHSINI.cdi.MaireService;
import org.MOUHSINI.model.Maire;

@Path("maire")
public class MaireRS {

	@Inject
	private MaireService maireservice;

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findById(@PathParam("id") int id) {

		Maire m = maireservice.findById(id);

		return Response.ok().entity(m).build();
	}

}
