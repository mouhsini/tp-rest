package org.MOUHSINI.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.MOUHSINI.model.Commune;

@Path("commune")
public class exo3 {

	@GET @Path("{nom}/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response findById(
			@PathParam("nom") String nom ,@PathParam("id") String id) {
		Commune c = new Commune(id,nom);
		return Response
				.ok()
				.entity(c)
				.build();
	}	
}
