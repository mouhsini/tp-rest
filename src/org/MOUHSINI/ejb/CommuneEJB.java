package org.MOUHSINI.ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.MOUHSINI.model.Commune;

@Stateless
public class CommuneEJB {
	
	@PersistenceContext(unitName = "tp-jpa-ee")
	private EntityManager em;
	
	public Commune findById(String codepostal) {
		return em.find(Commune.class, codepostal);
	}

}
