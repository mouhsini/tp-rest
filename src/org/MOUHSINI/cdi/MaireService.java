package org.MOUHSINI.cdi;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.MOUHSINI.model.Maire;


public class MaireService {

	@PersistenceContext(unitName = "tp-jpa-ee")
	private EntityManager em;
	
	public Maire findById(int id) {
		return em.find(Maire.class, id);
	}
	
	
}
