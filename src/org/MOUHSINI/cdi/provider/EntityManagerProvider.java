package org.MOUHSINI.cdi.provider;


import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


public class EntityManagerProvider {

	@Produces
	@PersistenceContext(unitName = "tp-jpa-ee")
	private EntityManager em;
}
