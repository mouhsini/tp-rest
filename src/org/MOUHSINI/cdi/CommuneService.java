package org.MOUHSINI.cdi;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.MOUHSINI.model.Commune;

@Transactional
public class CommuneService {

	@PersistenceContext(unitName = "tp-jpa-ee")
	private EntityManager em;
	
	@Transactional
	public Commune findById(String codepostal) {
		return em.find(Commune.class, codepostal);
	}
	
	@Transactional
	public void create(Commune c) {
	
		em.persist(c);
	
	}
}
