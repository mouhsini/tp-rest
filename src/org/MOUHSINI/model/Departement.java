package org.MOUHSINI.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Persistence;
import javax.persistence.Query;

@Entity(name = "Departement")
public class Departement implements Serializable {

	@Id
	public String codeDepartement;

	@Column(length = 80, nullable = false)
	public String nom;

	@OneToMany(mappedBy = "departement")
	public Set<Commune> communes = new HashSet<>();


	public Departement() {
	}

	public Departement(String codeDepartement, String nom) {
		this.codeDepartement = codeDepartement;
		this.nom = nom;
	}

	public String getCodeDepartement() {
		return codeDepartement;
	}

	public void setCodeDepartement(String codeDepartement) {
		this.codeDepartement = codeDepartement;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public boolean addCommune(Commune commune) {
		return this.communes.add(commune);
	}

	@Override
	public String toString() {
		return "Departement [codeDepartement=" + codeDepartement + ", nom=" + nom + "]";
	}




}
