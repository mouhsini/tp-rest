package org.MOUHSINI.model;

import java.io.Serializable;
import java.util.List;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

@Entity(name = "Commune")
@Table(name = "Commune")
@XmlRootElement
@XmlType(propOrder= {"nom","population"})
@XmlAccessorType(XmlAccessType.FIELD)
public class Commune implements Serializable{
	
	@Id
	@XmlAttribute(name = "codepostal")
	public String codepostal;
	
	@Column(length = 80, nullable = false)
	@XmlElement
	public String nom;
	
	@Column
	public long population;
	
	@OneToOne(cascade = CascadeType.PERSIST)
	@XmlTransient
	@JsonbTransient
	public Maire maire;
	
	public Maire getMaire() {
		return maire;
	}

	public void setMaire(Maire maire) {
		this.maire = maire;
	}

	@ManyToOne(cascade = CascadeType.PERSIST)
	@XmlTransient
	@JsonbTransient
	public Departement departement;
	
	
	
	public Departement getDepartement() {
		return departement;
	}

	public void setDepartement(Departement departement) {
		this.departement = departement;
	}

	public Commune() {
	}
	
	public Commune(String codepostal, String nom,long population) {

		this.codepostal = codepostal;
		this.nom = nom;
		this.population = population;
	}
	
	public Commune(String codepostal, String nom) {
		this.codepostal = codepostal;
		this.nom = nom;
	}

	public String getCodepostal() {
		return codepostal;
	}
	public void setCodepostal(String codepostal) {
		this.codepostal = codepostal;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}

	@Override
	public String toString() {
		return "Commune [codepostal=" + codepostal + ", nom=" + nom + ", maire=" + maire + "]";
	}

}
