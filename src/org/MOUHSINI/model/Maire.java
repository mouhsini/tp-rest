package org.MOUHSINI.model;

import java.io.Serializable;
import java.util.Date;

import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

@Entity(name = "maire")
@Table(name = "maire")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Maire implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@XmlAttribute(name = "id")
	public int id;
	
	@OneToOne(mappedBy = "maire")
	@XmlTransient
	@JsonbTransient
	public Commune commune;
	
	public Commune getCommune() {
		return commune;
	}

	public void setCommune(Commune commune) {
		this.commune = commune;
	}

	@Column(length = 80, nullable = false)
	@XmlElement
	public String nom;
	
	@Column(length = 80, nullable = false)
	@XmlElement
	public String prenom;
	
	@Column(length=8)
	@Enumerated(EnumType.STRING)
	@XmlElement
	public Civility civility;
	
	@Temporal(TemporalType.TIMESTAMP)
	@XmlElement
    public Date date_naissance;

	public Maire() {
	}
	
	public Maire(String nom, String prenom, Civility civility, Date date_naissance) {
		this.nom = nom;
		this.prenom = prenom;
		this.civility = civility;
		this.date_naissance = date_naissance;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public Civility getCivility() {
		return civility;
	}

	public void setCivility(Civility civility) {
		this.civility = civility;
	}

	public Date getDate_naissance() {
		return date_naissance;
	}

	public void setDate_naissance(Date date_naissance) {
		this.date_naissance = date_naissance;
	}

	@Override
	public String toString() {
		return "maire [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", civility=" + civility
				+ ", date_naissance=" + date_naissance + "]";
	}
	

}
